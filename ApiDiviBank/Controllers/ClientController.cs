﻿using System;
using System.Threading.Tasks;
using ApiDiviBank.Interfaces;
using ApiDiviBank.Models;
using Microsoft.AspNetCore.Mvc;

namespace ApiDiviBank.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ClientController : Controller
    {
        private IClient repo;

        public ClientController(IClient collection)
        {
            repo = collection;
        }

        [HttpGet]
        public async Task<IActionResult> getAll()
        {
            try
            {
                return Ok(await repo.getClients());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> save([FromBody] Client client)
        {
            if (client == null || client.birthday > DateTime.Now)
            {
                return BadRequest();
            }

            await repo.insert(client);
            return Created("Files added successfuly", true);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> getById(string id)
        {
            try
            {
                return Ok(await repo.GetClientById(id));
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> delete(string id)
        {
            await repo.delete(id);
            return NoContent();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> update([FromBody] Client client, string id)
        {
            if (client == null || client.birthday > DateTime.Now)
            {
                return BadRequest();
            }
            client.idClient = id;
            await repo.update(client);
            return Created("Data updated", true);
        }
    }
}
