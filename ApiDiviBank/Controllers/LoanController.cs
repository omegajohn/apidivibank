﻿using System;
using System.Threading.Tasks;
using ApiDiviBank.Interfaces;
using ApiDiviBank.Models;
using Microsoft.AspNetCore.Mvc;

namespace ApiDiviBank.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LoanController : Controller
    {
        private ILoan repo;

        public LoanController(ILoan collection)
        {
            repo = collection;
        }

        [HttpGet]
        public async Task<IActionResult> getAll()
        {
            try
            {
                return Ok(await repo.getAll());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> save([FromBody] Loan loan)
        {
            if (loan is null) return BadRequest();

            try
            {
                await repo.save(loan);
                return Created("Files added", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return BadRequest();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> delete(string id)
        {
            try
            {
                await repo.delete(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return NotFound();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> getById(string id)
        {
            try
            {
                return Ok(await repo.getById(id));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return NotFound();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> update([FromBody] Loan loan, string id)
        {
            if (loan is null) return BadRequest();
            try
            {
                loan.Id = id;
                await repo.update(loan);
                return Created("Files updated", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return NotFound();
        }
    }
}
