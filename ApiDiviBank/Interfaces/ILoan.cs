﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApiDiviBank.Models;

namespace ApiDiviBank.Interfaces
{
    public interface ILoan
    {
        Task<IEnumerable<Loan>> getAll();
        Task<Loan> getById(string id);
        Task save(Loan loan);
        Task update(Loan loan);
        Task delete(string id);
    }
}
