﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApiDiviBank.Models;

namespace ApiDiviBank.Interfaces
{
    public interface IClient
    {
        Task<IEnumerable<Client>> getClients();
        Task<Client> GetClientById(string id);
        Task insert(Client client);
        Task update(Client client);
        Task delete(string id);
    }
}
