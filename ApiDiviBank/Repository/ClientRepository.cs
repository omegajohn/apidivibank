﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApiDiviBank.Database;
using ApiDiviBank.Interfaces;
using ApiDiviBank.Models;
using Microsoft.EntityFrameworkCore;

namespace ApiDiviBank.Repository
{
    public class ClientRepository : IClient
    {
        private readonly DataContext db;

        public ClientRepository(DataContext context)
        {
            db = context;
        }

        public async Task delete(string id)
        {
            try
            {
                var search = await db.Clients.FindAsync(id);
                db.Clients.Remove(search);
                await db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public async Task<Client> GetClientById(string id)
        {
            try
            {
                return await db.Clients
                    .FirstOrDefaultAsync(x => x.idClient == id);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return null;
        }

        public async Task<IEnumerable<Client>> getClients()
        {
            try
            {
                return await db.Clients.ToListAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return null;
        }

        public async Task insert(Client client)
        {
            try
            {
                if (client.birthday < DateTime.Now)
                {
                    await db.AddAsync(client);
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public async Task update(Client client)
        {
            try
            {
                db.Entry(client).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
