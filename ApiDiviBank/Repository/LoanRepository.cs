﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiDiviBank.Database;
using ApiDiviBank.Interfaces;
using ApiDiviBank.Models;
using Microsoft.EntityFrameworkCore;

namespace ApiDiviBank.Repository
{
    public class LoanRepository : ILoan
    {
        private readonly DataContext db;

        public LoanRepository(DataContext context)
        {
            db = context;
        }

        public async Task delete(string id)
        {
            try
            {
                var search = await db.Loans.FindAsync(id);
                db.Loans.Remove(search);
                await db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public async Task<IEnumerable<Loan>> getAll()
        {
            try
            {
                return await db.Loans
                    .Include(x => x.Loans) 
                    .ToListAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return null;
        }

        public async Task<Loan> getById(string id)
        {
            try
            {
                return await db.Loans
                    .Include(x => x.Loans)
                    .FirstOrDefaultAsync(x => x.Id == id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return null;
        }

        public async Task save(Loan loan)
        {
            try
            {
                await db.Loans.AddAsync(loan);
                await db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public async Task update(Loan loan)
        {
            try
            {
                db.Entry(loan).State = EntityState.Modified;
                foreach (var l in loan.Loans)
                {
                    if (l.idDataLoan != 0)
                    {
                        db.Entry(l).State = EntityState.Modified;
                        await db.SaveChangesAsync();
                    }
                    else
                    {
                        db.Entry(l).State = EntityState.Added;
                        await db.SaveChangesAsync();
                    }
                    var loans = loan.Loans.Select(x => x.idDataLoan).ToList();
                    var toRemove = await db.DataLoans.Where(x => !loans.Contains(x.idDataLoan) &&
                    x.LoanId == loan.Id).ToListAsync();
                    db.RemoveRange(toRemove);
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
