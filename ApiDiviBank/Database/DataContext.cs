﻿using System;
using ApiDiviBank.Models;
using Microsoft.EntityFrameworkCore;

namespace ApiDiviBank.Database
{
    public class DataContext : DbContext
    {

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Loan> Loans { get; set; }
        public DbSet<DataLoan> DataLoans { get; set; }
    }
}
